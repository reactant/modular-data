package dev.reactant.modulardata

import dev.reactant.reactant.core.ReactantPlugin
import dev.reactant.reactant.core.component.Component
import dev.reactant.reactant.core.component.lifecycle.LifeCycleHook
import dev.reactant.reactant.core.dependency.layers.SystemLevel
import dev.reactant.reactant.extra.parser.GsonJsonParserService
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.ItemMeta
import org.bukkit.persistence.PersistentDataHolder
import org.bukkit.plugin.java.JavaPlugin


@ReactantPlugin(["dev.reactant.modulardata"])
class ModularData : JavaPlugin() {
    init {
        instance = this
    }

    companion object {
        @JvmStatic
        internal val logger: Logger = LogManager.getLogger("Reactant-ModularData")

        lateinit var instance: ModularData
    }
}

@Component
internal class ModularItemGsonGetter(
        private val gsonJsonParserService: GsonJsonParserService
) : LifeCycleHook, SystemLevel {
    override fun onEnable() {
        _gsonParser = gsonJsonParserService
    }

    companion object {
        private var _gsonParser: GsonJsonParserService? = null
        val gsonParser
            get() = _gsonParser
                    ?: throw IllegalStateException("Gson parse only available after System Level initialized")
    }
}

private fun ItemStack.getOrCreateItemMeta(): ItemMeta {
    if (!this.hasItemMeta()) {
        if (this.type === Material.AIR) throw UnsupportedOperationException("Air do not have item meta")
        this.itemMeta = Bukkit.getItemFactory().getItemMeta(this.type)
    }
    return this.itemMeta!!
}

/**
 * The changes of the itemMeta after this accessor created will be lost once the module changes commited
 * Base on the above rule:
 *
 *      - you should not save the this accessor for further use;
 *
 *      - you should not do any itemMeta changes inside .mutate{ }, but you can do the following:
 *          val exampleModule = itemStack.modules.getModule<Example>();
 *          /** some ItemMeta changes of the itemStack **/
 *          itemStack.modules.upsert(exampleModule)
 *
 */
val ItemStack.modules: DataModulesAccessor
    get() {
        val itemMeta = this.getOrCreateItemMeta()
        return DataModulesAccessor(
                { itemMeta.persistentDataContainer }, { this.setItemMeta(itemMeta) }
        )
    }

val PersistentDataHolder.modules get() = DataModulesAccessor({ this.persistentDataContainer }, {})

